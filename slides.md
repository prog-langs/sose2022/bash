---
tags: Bash,Vortragsreihe Programmiersprachen,Programmiersprache,Shell,POSIX,prog-langs
slideOptions:
  transition: slide
date: 2022-05-17
url: https://gitlab.gwdg.de/prog-langs/sose2022/bash
slides: https://prog-langs.pages.gwdg.de/sose2022/bash
pad_url: https://pad.gwdg.de/4lxBE6AMQoq5wi_AtZgoBg
pad_slides: https://pad.gwdg.de/p/oqySO1uiu#/
vortragsreihe: https://prog-langs.pages.gwdg.de/schedule/

---

<!--
Anforderung: Zunächst gibt es immer einen theoretischen Teil, der auf die Konzepte und Alleinstellungsmerkmale eingeht und mindestens 35 Minuten umfasst. Für die meisten Sprachen folgt auch noch ein zweiter, praktischer Teil in dem ihr die Gelegenheit habt, die Sprache selbst einmal auszuprobieren.

Abstract: Bash kennen sicherlich alle von euch als Shell um andere Programme auszuführen. Dass Bash als Sprache aber sehr viel mehr kann  und dass  man damit tatsächlich auch komplexere Skripte programmieren kann, könnt ihr in diesem Vortrag herausfinden. Eine Programmiersprache mit  ein paar mächtigen Konzepten, mehreren seltsamen Eigenheiten und sehr vielen Strings.

Themen:
- Basic erklärung der Syntax
- Funktionen
- @
- Pipe
- [ / test
- File IO
- stdout / stderr
- set
-
-->


Vortragsreihe Programmiersprachen

<br />

# BASH

<small><i class="fa fa-user"></i> Jake <i class="fa fa-calendar"></i> 17.05.2022  </small>

Note:
Hi, ich bin Jake und ich zeige euch heute ein paar interessante Aspekte der BASH Programmiersprache.

----

BASH ist ein ***mächtige*** Programmiersprache.

Note:
Ich möchte euch heute davon überzeugen, dass BASH eine mächtige Programmiersprache ist.

Es ist nicht unbedingt eine
- gute,
- sichere, oder
- sinnvolle
Programmiersprache.

Aber eine mächtige.

----

Nicht POSIX konform.

Note:
Es geht heute nicht um POSIX konforme Skripte, sondern BASH Skripte.

---

## Basics

- Prozedural
- Dateiendung: `.sh`
- Shebang: `#!/bin/bash`
- Der Rest der Datei sind Befehle

Note:
Was genau Befehle sind, wird gleich erklärt.

----

```bash
#!/bin/bash
echo "Hi"
echo "Ich"
echo "bin"
echo "Jake"
```

----


## Strings, STRINGS, ÜBERALL SIND STRINGS

Note:
Programme müssen selber wissen, wann sie Daten nicht als String interpretieren.

----

```bash
foo="bar"
echo "$foo"
echo "${foo}"
```

Note:
Alle Variablen sind globale Strings. (aber es gibt auch Ausnahmen)


----

```bash
while [ "schweine" != "fliegen" ]
do
    echo "Ich warte auf fliegende Schweine"
    sleep 1
done
```

Note:
`until` gibt es auch

----

```bash
for i in {1..5}
do
    echo "Runde: $i"
done
```

Note:

Leerzeichen seperierte Liste.
Also geht auch *.txt.

----

```bash
for f in *.txt
do
    echo "Tolle text datei: $f"
done
```

----

```bash
if [ "hallo" = "welt" ]; then
    echo "Aussage ist wahr"
elif [ "hallo" = "universum" ]; then
    echo "Andere Aussage ist auch wahr"
else
    echo "Beide Aussagen sind falsch"
fi
```

---

## Was sind Befehle?

----

### Programm

- $PATH
- z.B: cowsay, lolcat, ls, oder firefox

----

### Builtin

z.B. for, if, do, alias, etc...

----

### Alias

```bash
alias ll='ls -l'

ll
```

----

### Funktion

```bash
function foo() {
    echo "Hi, ich bin foo"
}

foo
```

Note:
Funktionen in BASH sind quasi mini BASH Skripte.

Die Klammern sind immer leer.

---

## IO von Befehlen

Note:
Funktionen in den meisten Programmiersprachen bekommen Argumente übergeben und geben
Rückgabewerte zurück. Bei BASH ist das anders, da Programme meist mehr haben.

----

### Input

- Umgebungsvariablen
- StdIn
- Argumente


----

### Output

- StdOut
- StdErr
- Rückgabewert als Integer (0 = SUCCESS)

Note:
Das hat folgen für Funktionen.

---


## IO lesen, schreiben & verändern

Note:
Einer der großen stärken von BASH.

----

### Argumente lesen

```bash
echo hallo welt
```

- `$@`: Liste aller Argumente -> "hallo" "welt"
- `$*`: String aller Argumente -> "hallo welt"
- `$#`: Anzahl Argumente -> 2
- `$0`: Befehl -> echo
- `$1`: erstes Argument -> "hallo"
- `$2`: zweites Argument -> "welt"
- `$n`: n-tes Argument


Note:


Funktioniert über Variablen.
Aber $@ ist besonders.

Ist lokal bei Funktionen.
Einzige standardmäßig nicht globale Variablenart.

----

### PIPEs

StdOut von a -> StdIn von b

```bash
echo "Hallo Welt" | cowsay | lolcat
```

Note:
Ermöglicht die Verkettung von Befehlen.
Dank "do one thing and do it well" sehr mächtig.

Wichtig:  Das ist keine Funktionsverkettung im üblichen Sinne!

----

### In Datei schreiben

StdOut -> Datei

```bash
echo "Hallo Welt" > hallo.txt
echo "Häng das auch noch dran." >> hallo.txt
```

----

### Nach StdErr schreiben

StdOut -> StdErr

```bash
echo "FEHLER FEHLER" >&2 | lolcat
```

Note:
File Descriptor

0: in
1: out
2: err

----

### Nach StdOut schreiben

StdErr -> StdOut

```bash
fehlerhafter_befehl 2>&1 | lolcat
```

----

### Aus Datei lesen

Datei -> StdIn

```bash
lolcat < hallo.txt
```

----

### Befehl als Argument

StdOut -> String

```bash
echo "Hallo $(echo "welt")"
```

----

### Befehl als Dateiargument

StdOut -> Pipe -> String

```bash
cat <(echo "welt")
```

----

### Rückgabewert setzen

```bash
return 0
```

----

Und noch mehr...

---

## Lokale Variablen

```bash
function foo() {
    local yeet
    yeet="Deletus"
    echo "$yeet"
}

yeet="Yeetus"
foo
echo "$yeet"
```

---

## Multithreading

```bash
firefox &
echo hi
```

---

## Test

Kennst du schon `/usr/bin/[` ?

---

## Quellen

- https://en.wikipedia.org/wiki/Bash_(Unix_shell)
- http://www.gnu.org/software/bash/manual/html_node
- http://www.gnu.org/software/bash/manual/html_node/Bash-POSIX-Mode.html#Bash-POSIX-Mode
- https://mywiki.wooledge.org/Bashism
- https://linuxize.com/post/bash-functions/
- https://en.wikipedia.org/wiki/Process_substitution
- Jahre an Erfahrung

---

## Ende

Slides: https://gitlab.gwdg.de/prog-langs/sose2022/bash



















