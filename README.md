# BASH Vortrag

BASH Vortrag aus der [Vortragsreihe Programmiersprachen](https://prog-langs.pages.gwdg.de/schedule/)

SoSe2022

## Quellcode

Den Quellcode der Slides findest du in [slides.md](slides.md) oder im [HedgeDoc Pad](https://pad.gwdg.de/4lxBE6AMQoq5wi_AtZgoBg).

## Slides

Die gerenderten Slides findest du entweder [hier](https://prog-langs.pages.gwdg.de/sose2022/bash) oder [via dem HedgeDoc Pad](https://pad.gwdg.de/p/oqySO1uiu#/).

